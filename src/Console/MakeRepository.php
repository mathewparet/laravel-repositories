<?php

namespace mathewparet\LaravelRepositories\Console;

use Illuminate\Support\Str;
use InvalidArgumentException;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;

class MakeRepository extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repository {model} {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a repository class';

    protected $type = 'RepositoryClass';

    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);

        $modelPath = $this->parseModel($this->argument('model'));

        $search = [
            'DummyRepositoryClass',
            '{{ model_path }}',
            '{{ model_class }}',
            '{{ repository_class }}',
            '{{ repository_contract_path }}',
            '{{ repository_path }}',
            '{{ namespacedModel }}',
        ];

        $replacements = [
            $this->argument('name'),
            $modelPath,
            class_basename($modelPath),
            config('repositories.path.repositories').'\\'.$this->argument('name'),
            config('repositories.path.contracts'),
            config('repositories.path.repositories'),
            $modelPath,
        ];

        return str_replace($search, $replacements, $stub);
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/DummyRepositoryClassWithContract.stub';
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return config('repositories.path.repositories');
    }

    /**
     * Get the fully-qualified model class name.
     *
     * @param  string  $model
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    protected function parseModel($model)
    {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $model)) {
            throw new InvalidArgumentException('Model name contains invalid characters.');
        }

        return $this->qualifyModel($model);
    }
}
