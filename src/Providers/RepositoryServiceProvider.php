<?php
namespace mathewparet\LaravelRepositories\Providers;

use Illuminate\Support\ServiceProvider;
use mathewparet\LaravelRepositories\Console\MakeRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    const CONFIG_FILE = __DIR__.'/config/repositories.php';

    public function boot()
    {
        $this->definePublishableComponents();
    }

    public function register()
    {
        $this->mergeConfigFrom(self::CONFIG_FILE, 'repositories');

        $this->app->bind('command.make:repositoru', MakeRepository::class);

        $this->commands([
            'command.make:repositoru'
        ]);

        foreach(config('repositories.repositories', []) as $contract => $class)
        {
            $this->app->singleton($contract, $class);
        }
    }

    private function definePublishableComponents()
    {
        $this->publishes([
            self::CONFIG_FILE => config_path('repositories.php'),
        ], 'laravel-repositories');
    }
}