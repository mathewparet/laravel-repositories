<?php

return [
    /**
     * Repository contract to Repository class mapping
     */
    'repositories' => [
        // App\Contracts\UserRepository::class => App\Repositories\UserRepository::class,
    ],

    'path' => [
        /**
         * Namespace to be used for repository contracts
         */
        'contracts' => "App\\Contracts",

        /**
         * Namespace to beused for repository classes
         */
        "repositories" => "App\\Repositories",
    ],
];