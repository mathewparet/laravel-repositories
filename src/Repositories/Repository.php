<?php
namespace mathewparet\LaravelRepositories\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use mathewparet\LaravelRepositories\Contracts\Repository as ContractsRepository;

class Repository implements ContractsRepository
{
    use AuthorizesRequests;

    protected \Illuminate\Database\Eloquent\Model $model;

    protected string $model_class;

    public function __construct()
    {
        $this->model = resolve($this->model_class);
    }

    private function getModelName($plural = true)
    {
        $class_base_name = class_basename($this->model);

        return $plural ? Str::plural($class_base_name) : $class_base_name;
    }

    protected function writeDebugLog($message, $attributes = [], $plural = true)
    {
        Log::debug($message.' '.$this->getModelName($plural), $attributes);
    }
    
    protected function writeInfoLog($message, $attributes = [], $plural = true)
    {
        Log::info($message.' '.$this->getModelName($plural), $attributes);
    }
    
    protected function writeErrorLog($message, $attributes = [], $plural = true)
    {
        Log::error($message.' '.$this->getModelName($plural), $attributes);
    }

    public function forceDelete($id)
    {
        $this->writeDebugLog('Force deleting');

        return tap($this->model::onlyTrashed()->findOrFail($id), function($model) {
            $this->authorize('forceDelete', $model);

            $model->forceDelete();
        });
    }

    public function restore($id)
    {
        $this->writeDebugLog('Restoring');

        return tap($this->model::onlyTrashed()->findOrFail($id), function($model) {
            $this->authorize('restore', $model);

            $model->restore();
        });
    }

    public function list()
    {
        $this->writeDebugLog('Listing all');

        $this->authorize('viewAny', $this->model);

        return $this->model;
    }

    public function trashed()
    {
        $this->writeDebugLog('Listing all trashed');

        $this->authorize('viewAny', $this->model);

        return $this->model::onlyTrashed();
    }

    public function findById($id): Model
    {
        $this->writeDebugLog('Viewing', compact('id'), plural: false);

        return tap($this->model::findOrFail($id), function($model) {
            $this->authorize('view', $model);
        });
    }

    public function update($id, $attributes = [])
    {
        $this->writeDebugLog('Updating', compact('id'));

        return tap($this->model::findOrFail($id), function($model) use($attributes) {
            $this->authorize('update', $model);

            $model->update($attributes);

            $this->writeInfoLog('Updated', ['id' => $model->id], false);
        });
    }

    public function add($attributes = []): Model
    {
        $this->writeDebugLog('Creating');

        $this->authorize('create', $this->model);

        return tap($this->model::create($attributes), function($model) {
            $this->writeInfoLog('Created', ['id' => $model->id], false);
        });
    }

    public function delete($id)
    {
        $this->writeDebugLog('Deleting', compact('id'), false);

        return tap($this->model::findOrFail($id), function($model) {
            $this->authorize('delete', $model);

            $model->delete();
        });
    }
}