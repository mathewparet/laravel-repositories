<?php
namespace mathewparet\LaravelRepositories\Contracts;

use Illuminate\Database\Eloquent\Model;

interface Repository
{
    /**
     * Get all records from the repository
     * 
     * @return \Illuminate\Contracts\Database\Eloquent\Builder
     */
    public function list();

    /**
     * Get all trashed records
     * 
     * @return \Illuminate\Contracts\Database\Eloquent\Builder
     */
    public function trashed();

    /**
     * Find a record by ud
     * 
     * @param mixed $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findById($id): Model;

    /**
     * Add a record to the repository
     * 
     * @param array $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function add($attributes = []): Model;

    /**
     * Delete a record from the repository
     * 
     * @param mixed $id
     * @return bool|\Illuminate\Database\Eloquent\Model
     */
    public function delete($id);

    /**
     * Force delete a record from the repository
     * 
     * @param mixed $id
     * @return bool|\Illuminate\Database\Eloquent\Model
     */
    public function forceDelete($id);

    /**
     * Restore record from the trash
     * 
     * @param mixed $id
     * @return bool|\Illuminate\Database\Eloquent\Model
     */
    public function restore($id);

    /**
     * Update a record in the repository
     * 
     * @param mixed $int
     * @param array $attributes
     * @return bool|\Illuminate\Database\Eloquent\Model
     */
    public function update($id, $attributes = []);
}